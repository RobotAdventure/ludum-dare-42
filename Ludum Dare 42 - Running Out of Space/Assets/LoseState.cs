﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoseState : MonoBehaviour
{

    public FloatVariable m_oxygen;
    public FloatVariable m_hull;
    public FloatVariable m_score;
    public FloatVariable m_shield;
    public FloatVariable m_fuel;

    private void Start()
    {
        m_score.value = m_score.m_initialValue;

    }

    private void Update()
    {
        if(m_oxygen.value <= 0 || m_hull.value <= 0 ) PreLoadScoreScreen();

        if (Input.GetKeyDown(KeyCode.O)) m_oxygen.value = 1.0f;
        if (Input.GetKeyDown(KeyCode.H)) m_hull.value = 0.0f;
    }

    private void PreLoadScoreScreen()
    {
        //Reset Values
        m_oxygen.value = m_oxygen.m_initialValue;
        m_hull.value = m_hull.m_initialValue;

        m_shield.value = m_shield.m_initialValue;
        m_fuel.value = m_fuel.m_initialValue;

        //m_oxygen.m_event = null;


        SceneManager.LoadScene(2);
        //Invoke("LoadScoreScreen", 1.0f);


    }

    public void LoadScoreScreen()
    {
        SceneManager.LoadScene(2);
    }

    public void OnDisable()
    {
        Debug.Log("LOSE STATE DISABLED");
    }

    public void OnDestroy()
    {
        
    }

}
