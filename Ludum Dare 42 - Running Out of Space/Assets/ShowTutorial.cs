﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTutorial : MonoBehaviour
{
    public GameObject m_mainMenu;
    public GameObject m_tutorial;

    public void ShowTutorialButton()
    {
        m_mainMenu.SetActive(false);
        m_tutorial.SetActive(true);
    }
}
