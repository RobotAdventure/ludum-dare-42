﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreScreen : MonoBehaviour
{

    public FloatVariable m_score;
    public Text m_scoreText;

    private void Start()
    {
        m_scoreText.text = ((int)Mathf.Floor(m_score.value)).ToString();
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }
}
