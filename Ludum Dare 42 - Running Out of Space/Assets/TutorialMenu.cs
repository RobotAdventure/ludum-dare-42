﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialMenu : MonoBehaviour
{
    public Text m_tutText;
    public RawImage m_tutImage;

    public TutorialMenuSO [] m_tutData;

    public GameObject m_nextButton;
    public GameObject m_startButton;
    public GameObject m_backButton;

    private int m_tutIndex = 0;


    public void Next()
    {
        if(m_tutIndex < m_tutData.GetUpperBound(0))
        {
            m_tutIndex++;
            SetTut(m_tutData[m_tutIndex]);

            if(m_tutIndex == m_tutData.GetUpperBound(0))
            {
                m_nextButton.SetActive(false);
                m_startButton.SetActive(true);
            }

            m_backButton.SetActive(true);
        }
        
    }

    public void Back()
    {
        if(m_tutIndex > 0)
        {
            m_tutIndex--;
            SetTut(m_tutData[m_tutIndex]);

            if (m_tutIndex == 0)
            {
                m_backButton.SetActive(false);
            }

            m_nextButton.SetActive(true);
            m_startButton.SetActive(false);
        }
    }

    void SetTut(TutorialMenuSO data)
    {
        m_tutText.text = data.m_text;
        m_tutImage.texture = data.m_picture;
    }
}
