﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CargoObject))]
public class CargoObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CargoObject tempCargoObject;
        CargoObjectPiece[] pieces;
        SpriteRenderer tempSpriteRenderer;
        GameObject prefabRef;

        if(GUILayout.Button(new GUIContent("Apply Color")))
        {
            tempCargoObject = target as CargoObject;
            pieces = tempCargoObject.gameObject.GetComponentsInChildren<CargoObjectPiece>();

                    
            //Undo.RecordObject(target, "Cargo Type Change");
            (target as CargoObject).ApplyColor();

//            prefabRef = PrefabUtility.FindPrefabRoot(target as GameObject);
            //prefabRef = PrefabUtility.GetOutermostPrefabInstanceRoot(target);
         //   prefabRef = PrefabUtility.GetCorrespondingObjectFromOriginalSource<GameObject>(tempCargoObject.gameObject);
         //   PrefabUtility.ReplacePrefab((target as CargoObject).gameObject, prefabRef);

           foreach(CargoObjectPiece piece in pieces)
            {
                Undo.RecordObject(piece, "Changed Color of " + piece.name);
                tempSpriteRenderer = piece.gameObject.GetComponent<SpriteRenderer>();
                

            }

            GameObject temp = new GameObject("temp");
            temp.transform.parent = tempCargoObject.transform;
            //DestroyImmediate(temp);
            
        }
    }
}
