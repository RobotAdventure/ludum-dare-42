﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpaceSpawner))]
public class SpaceSpawnerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button(new GUIContent("Create Chunk")))
        {
            (target as SpaceSpawner).CreateChunk();
        }

        if (GUILayout.Button(new GUIContent("Create Initial Spawn")))
        {
            (target as SpaceSpawner).SetupSpace();
        }
    }
}
