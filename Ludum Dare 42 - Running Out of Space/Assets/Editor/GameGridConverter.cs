﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

//Unity Inspector Tool
//Feed in a unity Tilemap and it poops out a game grid prefab

public class GameGridConverter : EditorWindow
{
    

    public Grid m_targetGrid;
    public GameGrid m_targetGameGrid;
    public GameSpace m_selectedGameSpace;
    private List<Tilemap> m_gridTileMaps = new List<Tilemap>();
    private Editor m_gameSpaceEditor;
    private SerializedObject m_serlizedObjectGameSpace;

    [MenuItem("Window/Game Grid Converter")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(GameGridConverter));
    }

    private void OnEnable()
    {
        GridSelection.gridSelectionChanged += UpdateGridSelection;
        UpdateGridSelection();
    }

    private void Update()
    {
        Repaint();
    }

    void OnGUI()
    {
        m_targetGrid = EditorGUILayout.ObjectField(new GUIContent("Target Grid"), m_targetGrid, typeof(Grid), true) as Grid;
        m_targetGameGrid = EditorGUILayout.ObjectField(new GUIContent("Target GameGrid"), m_targetGameGrid, typeof(GameGrid), true) as GameGrid;

        if (GUILayout.Button("Generate Game Grid"))
        {
            GenerateGameGrid(m_targetGrid.gameObject);
        }

        EditorGUILayout.Space();
        if(m_targetGameGrid != null)
        {
            if(m_selectedGameSpace == null) //Check if selection is null. If it is, update it
            {
                UpdateGridSelection();
                if (m_selectedGameSpace == null) return; //If it is still null, don't show the property
            }
            GUILayout.Label(new GUIContent(m_selectedGameSpace.name));
           // m_gameSpaceEditor.DrawDefaultInspector();

            if (GUILayout.Button("Combatant Occupy Check"))
            {
                CombatantOccupyCheck();
            }
        }


        



    }
    #region GridConverter
    private void GenerateGameGrid(GameObject target)
    {
        m_targetGrid = target.GetComponent<Grid>();
        m_targetGameGrid = target.GetComponent<GameGrid>();
        

        if(m_targetGrid == null)
        {
            Debug.LogWarning("CONVERTER - Target Object is not a grid");
            return;
        }

        if(m_targetGameGrid != null)
        {
            Debug.LogWarning("CONVERTER - Target already has a game grid, aborting");
            return;
        }

        Debug.Log("CONVERTER - Getting Grid Tilemaps");
        m_gridTileMaps = PullTilemapsFromGrid(m_targetGrid);

        Debug.Log("CONVERTER - Calculating Grid Size");
        BoundsInt gridBounds = GetGameGridSize(m_gridTileMaps);
        Vector3Int gridSize = gridBounds.size;

        Debug.Log("CONVERTER - Adding Game Grid");
        m_targetGameGrid = target.AddComponent<GameGrid>();
        m_targetGameGrid.m_gameSpaces = new Array3dGameSpace(gridSize);
        m_targetGameGrid.m_gridBounds = gridBounds;
        m_targetGameGrid.m_grid = m_targetGrid;
        m_targetGameGrid.m_tileMaps = m_gridTileMaps;
        m_targetGameGrid.CalculateOffset();

        Debug.Log("CONVERTER - Populating Array");
        CreateGameSpaces(ref m_targetGameGrid.m_gameSpaces);

        Debug.Log("CONVERTER - Compiling Modifiers");
        ConvertTilemapsToGameSpaces(m_targetGameGrid.m_gameSpaces, m_gridTileMaps, m_targetGrid, m_targetGameGrid);
    }

    private List<Tilemap> PullTilemapsFromGrid(Grid targetGrid)
    {
        List<Tilemap> tilemaps = new List<Tilemap>();

        foreach(Tilemap map in targetGrid.GetComponentsInChildren<Tilemap>())
        {
            tilemaps.Add(map);
        }

        Debug.Log("CONVERTER - Number of Tilemaps: " + tilemaps.Count);

        return tilemaps;
    }

    private BoundsInt GetGameGridSize(List<Tilemap> tilemaps)
    {
        BoundsInt gridBounds = new BoundsInt();
        foreach(Tilemap map in tilemaps)
        {
            LargestBoundsCalculation(ref gridBounds, map.cellBounds);
        }

        Debug.Log("CONVERTER - Size of Tilemaps: " + gridBounds.size);

        return gridBounds;
    }

    private BoundsInt LargestBoundsCalculation( ref BoundsInt runningTotal, BoundsInt compared)
    {
        //Compare  Min Bounds
        runningTotal.x = Mathf.Min(runningTotal.x, compared.x);
        runningTotal.y = Mathf.Min(runningTotal.y, compared.y);
        runningTotal.z = Mathf.Min(runningTotal.z, compared.z);

        runningTotal.xMax = Mathf.Max(runningTotal.max.x, compared.max.x);
        runningTotal.yMax = Mathf.Max(runningTotal.max.y, compared.max.y);
        runningTotal.zMax = Mathf.Max(runningTotal.max.z, compared.max.z);

        return runningTotal;
    }

    private void CreateGameSpaces(ref Array3dGameSpace spaces)
    {
        //Populate the 3D array of Game Spaces
        GameSpace tempGameSpace;
        for (int x = 0; x < spaces.size.x; x++)
        {
            for (int y = 0; y < spaces.size.y; y++)
            {
                for (int z = 0; z < spaces.size.z; z++)
                {
                    //tempGameSpace = ScriptableObject.CreateInstance<GameSpace>();
                    tempGameSpace = new GameSpace();
                    tempGameSpace.name = "[" + x + "," + y + "," + z + "]";
                    spaces[x, y, z] = tempGameSpace;
                    tempGameSpace.m_gridPosition = m_targetGameGrid.GameSpacePositionToTilePosition(new Vector3Int(x, y, z));
                    tempGameSpace.m_gridOwner = m_targetGameGrid;
                }
            }
        }
    }

    private void ConvertTilemapsToGameSpaces(Array3dGameSpace spaces, List<Tilemap> tilemaps, Grid grid, GameGrid targetGameGrid )
    {
        //Go through each space and check the tiles in that coordiante to pull modifiers.
        BoundsInt mapBounds;
        bool emptySpace = true;
        TileBase currentTile;
        Vector3 currentTileWorldPosition;
        GameSpace tempGameSpace;

        for(int x = 0; x < spaces.size.x; x++)
        {
            for (int y = 0; y < spaces.size.y; y++)
            {
                for (int z = 0; z < spaces.size.z; z++)
                {
                    //Run through all the maps and check if they have a tile at the location
                    //Use World Position to get around Tilemaps having differnt local coordinates
                    //If none of the maps have a tile there, null the space
                    //Else, add all modifiers from that tile if it's a game tile

                    emptySpace = true; 
                    currentTileWorldPosition = grid.CellToWorld(targetGameGrid.GameSpacePositionToTilePosition(new Vector3Int(x, y, z)));
                    foreach(Tilemap map in tilemaps)
                    {
                        currentTile = map.GetTile(map.WorldToCell(currentTileWorldPosition));
                        if (currentTile == null) continue;
                        else
                        {
                            emptySpace = false;
                            tempGameSpace = spaces[x, y, z];
                            ProcessTileModifiers(ref tempGameSpace, currentTile);
                        }
                    }
                    if (emptySpace) spaces[x, y, z] = null; //No tile so release the game space

                }
            }
        }
    }

    private void ProcessTileModifiers(ref GameSpace space, TileBase tile)
    {
        
        GameTile gameTile;
        if(tile is GameTile)
        {
            gameTile = tile as GameTile;
            foreach(GameSpaceModifier mod in gameTile.m_modifiers)
            {
                space.m_modifiers.Add(mod);
                //TODO Duplicate the mods that require duplication and instantiate their prefabs
            }

        }
        //else do nothing because it's just a visual tile
    }
    #endregion

    public void UpdateGridSelection()
    {
        if (m_targetGameGrid == null) return;


        m_selectedGameSpace = m_targetGameGrid.TilePositionToGameSpace(GridSelection.position.min);

        if (m_selectedGameSpace == null) return;
        //m_serlizedObjectGameSpace = new SerializedObject(m_selectedGameSpace);
        //m_gameSpaceEditor = Editor.CreateEditor(m_selectedGameSpace);
        Repaint();
        Debug.Log("Grid Selection Changed");
    }

    public void CombatantOccupyCheck()
    {
        //if (m_targetGameGrid == null) return;

        //Combatant[] combatants = Object.FindObjectsOfType<Combatant>();
        //GameSpace tempGameSpace;
        //foreach(Combatant com in combatants)
        //{
        //    if(com.m_gameSpace == null)
        //    {
        //        tempGameSpace = m_targetGameGrid.WorldPositionToGameSpace(com.transform.position);
        //        com.OccupyNewGameSpace(tempGameSpace);
        //    }
        //}
    }

}
