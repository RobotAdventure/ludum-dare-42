﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class CargoConverter :  EditorWindow
{
    public Grid m_targetGrid;
    public Tilemap m_targetTilemap;
    public GameObject m_cargoObjectPrefab;
    public GameObject m_cargoPiecePrefab;


    [MenuItem("Window/Cargo Part Converter")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(CargoConverter));
    }

    private void OnEnable()
    {

    }

    private void Update()
    {
        Repaint();
    }

    void OnGUI()
    {
        m_targetGrid = EditorGUILayout.ObjectField(new GUIContent("Target Grid"), m_targetGrid, typeof(Grid), true) as Grid;
        m_targetTilemap = EditorGUILayout.ObjectField(new GUIContent("Target Tilemap"), m_targetTilemap, typeof(Tilemap), true) as Tilemap;
        m_cargoObjectPrefab = EditorGUILayout.ObjectField(new GUIContent("Cargo Object Prefab"), m_cargoObjectPrefab, typeof(GameObject), true) as GameObject;
        m_cargoPiecePrefab = EditorGUILayout.ObjectField(new GUIContent("Cargo Piece Prefab"), m_cargoPiecePrefab, typeof(GameObject), true) as GameObject;

        if (GUILayout.Button("Generate Cargo Object"))
        {
            GenerateCargoObject(m_targetGrid.gameObject);
        }

    }


    private void GenerateCargoObject(GameObject target)
    {
        m_targetGrid = target.GetComponent<Grid>();

        if (m_targetGrid == null)
        {
            Debug.LogWarning("CONVERTER - Target Object is not a grid");
            return;
        }

        if (m_targetTilemap == null)
        {
            Debug.LogWarning("CONVERTER - Does not have a tilemap");
            return;
        }

        ConvertTilemapToCargo(m_targetTilemap, m_targetGrid);


    }

    private List<Tilemap> PullTilemapsFromGrid(Grid targetGrid)
    {
        List<Tilemap> tilemaps = new List<Tilemap>();

        foreach (Tilemap map in targetGrid.GetComponentsInChildren<Tilemap>())
        {
            tilemaps.Add(map);
        }

        Debug.Log("CONVERTER - Number of Tilemaps: " + tilemaps.Count);

        return tilemaps;
    }

    private BoundsInt GetGameGridSize(List<Tilemap> tilemaps)
    {
        BoundsInt gridBounds = new BoundsInt();
        foreach (Tilemap map in tilemaps)
        {
            LargestBoundsCalculation(ref gridBounds, map.cellBounds);
        }

        Debug.Log("CONVERTER - Size of Tilemaps: " + gridBounds.size);

        return gridBounds;
    }

    private BoundsInt LargestBoundsCalculation(ref BoundsInt runningTotal, BoundsInt compared)
    {
        //Compare  Min Bounds
        runningTotal.x = Mathf.Min(runningTotal.x, compared.x);
        runningTotal.y = Mathf.Min(runningTotal.y, compared.y);
        runningTotal.z = Mathf.Min(runningTotal.z, compared.z);

        runningTotal.xMax = Mathf.Max(runningTotal.max.x, compared.max.x);
        runningTotal.yMax = Mathf.Max(runningTotal.max.y, compared.max.y);
        runningTotal.zMax = Mathf.Max(runningTotal.max.z, compared.max.z);

        return runningTotal;
    }

    private void ConvertTilemapToCargo(Tilemap tilemap, Grid grid)
    {
        //Set the cargo object to the grid's position
        //Run through all the maps and check if they have a tile at the location
        //For Each Tile, create a cargo piece
        //Child the piece to the cargo object
        //Set the piece to the position of the tile
        //Set the sprite of the object to the tile map sprite

        BoundsInt tilemapBounds = tilemap.cellBounds;
        GameObject tempCargoPiece;
        Tile tempTile;
        Vector3 tileSizeOffset = grid.cellSize * .5f;
        GameObject tempCargoObject = PrefabUtility.InstantiatePrefab(m_cargoObjectPrefab) as GameObject;

        tempCargoObject.transform.position = grid.transform.position;
        for (int x = tilemapBounds.min.x; x < tilemapBounds.xMax; x++)
        {
            for (int y = tilemapBounds.min.y; y < tilemapBounds.yMax; y++)
            {
                tempTile = tilemap.GetTile(new Vector3Int(x, y, 0)) as Tile;
                if (tempTile == null) continue;

                tempCargoPiece = PrefabUtility.InstantiatePrefab(m_cargoPiecePrefab) as GameObject;
                tempCargoPiece.transform.parent = tempCargoObject.transform;
                
                tempCargoPiece.transform.position = tilemap.CellToWorld(new Vector3Int(x, y, 0)) + tileSizeOffset;

                //Rotate
                tempCargoPiece.transform.rotation = tilemap.GetTransformMatrix(new Vector3Int(x, y, 0)).rotation;


                tempCargoPiece.GetComponent<SpriteRenderer>().sprite = tempTile.sprite;
                
            }            
        }

        CargoObjectPiece [] pieces = tempCargoObject.GetComponentsInChildren<CargoObjectPiece>();
        CargoObject tempCargoObjectScript = tempCargoObject.GetComponent<CargoObject>();
        tempCargoObjectScript.m_amount = 5 * pieces.Length;

    }

}
