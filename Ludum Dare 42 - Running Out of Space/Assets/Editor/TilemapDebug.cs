﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[ExecuteInEditMode]
public class TilemapDebug : MonoBehaviour
{
    public Tilemap m_tileMap;
    public Tile m_tile;
    public Quaternion m_tileRotation;

    public GameObject m_spawnedPrefab;

    private void Update()
    {
        m_tile = m_tileMap.GetTile(GridSelection.position.min) as Tile;
        m_tileRotation = m_tileMap.GetTransformMatrix(GridSelection.position.min).rotation;

    }

    public void SpawnPrefab()
    {
        
        PrefabUtility.InstantiatePrefab(m_spawnedPrefab);
    }
}
