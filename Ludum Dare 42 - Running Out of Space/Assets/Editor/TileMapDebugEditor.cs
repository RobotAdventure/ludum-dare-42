﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TilemapDebug))]
public class TileMapDebugEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TilemapDebug tempTileampDebug = target as TilemapDebug;

        if (GUILayout.Button(new GUIContent("SpawnPrefab")))
        {
            tempTileampDebug.SpawnPrefab();
        }
    }
}
