﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDamage : EntityPart
{
    public DamageEvent m_damageEvent;
    public int m_damage;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        m_damageEvent.Raise(new DamageEventInfo(this.gameObject, collision.collider.gameObject, m_damage));
    }


}
