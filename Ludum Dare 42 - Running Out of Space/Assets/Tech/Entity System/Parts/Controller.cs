﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : EntityPart
{
    ControllerBehavior m_behavior;

    public void Start()
    {
        m_behavior = Instantiate(m_behavior); //Instance the behavior
        m_behavior.Setup(this);
    }
}
