﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : EntityPart
{
    public DeathBehavior m_behavior;
    public bool isInvuln
    {
        get { return m_invulnStop > Time.time; }
    }

    public float m_damageInvulnTime = 1.0f;
    protected float m_invulnStop;

    public int m_health
    {
        get { return Mathf.RoundToInt(m_healthReference.value); }
    }

    public FloatVariable m_healthReference;
    public bool m_instanceHealth; //whether we need to duplicate health on Start

    public DamageEvent m_damageEvent;

    public virtual void Start()
    {
        m_behavior = Instantiate(m_behavior); //Instance the behavior
        m_behavior.Setup(this);
    }


    //public DeathBehabior m_deathBehavior;

    public void Awake()
    {
        if (m_healthReference == null) Debug.LogWarning("m_health on " + gameObject.name + " is unassigned.");
        else if(m_instanceHealth)
        {
            m_healthReference = Instantiate(m_healthReference);
        }


    }

    private void OnEnable()
    {
        //Register Event
        m_damageEvent.m_event += CheckDamageEvent;
    }

    private void OnDisable()
    {
        m_damageEvent.m_event -= CheckDamageEvent;
    }

    public void SetHealth(int value)
    {
        m_healthReference.value = (float)value;
    }

    public void CheckDamageEvent(DamageEventInfo info)
    {
        if (info.m_targetGameObject == this.gameObject) TakeDamage(info.m_damage);
    }

    public virtual void TakeDamage(int damage)
    {
        if (isInvuln) return;

        SetHealth(m_health - damage);
        if(m_health <= 0)
        {
            m_behavior.Execute();
        }
        else
        {
            SetInvulnTime(m_damageInvulnTime);
        }
    }

    public void SetInvulnTime(float time)
    {
        m_invulnStop = Time.time + time;
    }


}
