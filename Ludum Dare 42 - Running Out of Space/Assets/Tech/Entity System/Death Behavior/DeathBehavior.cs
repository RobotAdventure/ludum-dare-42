﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Entity/Death Behavior", fileName = "Death Behavior")]
public class DeathBehavior : EntityBehavior
{
    public override void Execute()
    {
        base.Execute();
        Destroy(m_owner);
    }

}
