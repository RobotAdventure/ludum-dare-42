﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBehavior : ScriptableObject
{
    protected EntityPart m_owner;

    public virtual void Setup(EntityPart part)
    {
        m_owner = part;
    }

    public virtual void Execute()
    {

    }

}
