﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Instead of having "player" and "enemy" scripts, you have universal entities that you build piecemeal from other scripts
public class GameEntity : MonoBehaviour
{
    public virtual T GetPart<T>() where T : EntityPart
    {
        return gameObject.GetComponent<T>();
    }    

    public virtual bool HasPart<T>() where T : EntityPart
    {
        T tempPart = gameObject.GetComponent<T>();
        if (tempPart == null) return false;
        else return true;
    }

    public virtual bool GetPart<T>(out T part) where T : EntityPart
    {
        bool hasPart = HasPart<T>();
        if (hasPart) part = GetPart<T>();
        else part = null;
        return hasPart;
    }

}
