﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GGETest : MonoBehaviour
{
    public BasicGlobalGameEvent m_clickEvent;


    void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Click"))
        {
            m_clickEvent.Raise(new GlobalGameEventInfo(gameObject));
        }
    }
}
