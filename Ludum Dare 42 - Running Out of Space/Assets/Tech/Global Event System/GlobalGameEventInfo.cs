﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalGameEventInfo
{
    public Object m_sourceGameObject;
    public Object m_targetGameObject;

    public GlobalGameEventInfo(Object source, Object target = null)
    {
        m_sourceGameObject = source;
        if (target != null) m_targetGameObject = target;
    }
}
