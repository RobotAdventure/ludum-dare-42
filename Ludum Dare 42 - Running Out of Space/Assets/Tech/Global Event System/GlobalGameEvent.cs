﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GlobalGameEvent <T> : ScriptableObject where T: GlobalGameEventInfo
{
    public delegate void EventInvoke(T info);
    public event EventInvoke m_event; 

    public virtual void Raise(T info)
    {
        if(m_event != null) m_event(info);

    }

}


