﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Global Game Event /Basic Event", fileName = "Global Game Event")]
public class BasicGlobalGameEvent : GlobalGameEvent<GlobalGameEventInfo>
{ }
