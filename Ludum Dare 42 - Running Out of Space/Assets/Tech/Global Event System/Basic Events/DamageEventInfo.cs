﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEventInfo : GlobalGameEventInfo
{
    public int m_damage;

    public DamageEventInfo(Object source, Object target, int damage) : base(source, target)
    {
        m_damage = damage;
    }

}
