﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Global Game Event /Damage Event", fileName = "Damage Event")]
public class DamageEvent : GlobalGameEvent<DamageEventInfo>
{ }
