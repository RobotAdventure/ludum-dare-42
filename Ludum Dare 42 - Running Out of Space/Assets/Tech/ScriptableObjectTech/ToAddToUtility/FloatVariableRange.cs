﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Float Variable Range", fileName = "FloatVariableRange")]
public class FloatVariableRange : FloatVariable
{
    public float m_initialMin;
    public float m_intialMax;

    [System.NonSerialized]
    public float m_runtimeMin;
    [System.NonSerialized]
    public float m_runtimeMax;

    public override float value
    {
        get { return m_runtimeValue; }
        set
        {
            base.value = value;
            m_runtimeValue = Mathf.Clamp(value, m_runtimeMin, m_runtimeMax);
        }
    }

    public override void OnAfterDeserialize()
    {
        base.OnAfterDeserialize();
        m_runtimeMin = m_initialMin;
        m_runtimeMax = m_intialMax;
    }
}
