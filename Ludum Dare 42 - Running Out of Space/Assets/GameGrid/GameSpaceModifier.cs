﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(menuName = "GameGrid/Modifier")]
public class GameSpaceModifier : ScriptableObject
{
    public bool m_needsInstance = false; //Weather local data needs to be tracked. If so, the scriptable object is duplicated for each game space
    public bool m_isInstance = false; //If duplication is required, tracks if this is an instance or not. Flagged true by the duplication process;

    public GameSpace m_spaceOwner;

    public GameObject m_attachedGameObjectPrefab;
    public GameObject m_attachedGameObject;

    public void Start()
    {
        if(m_needsInstance && m_isInstance && m_attachedGameObjectPrefab != null) //we are instanced so create the attached prefab
        {
            //TODO: Spawn the attached object in the right spot based off of tile;
        }
    }


	public GameSpaceModifier Duplicate(GameSpace owner)
    {
        GameSpaceModifier tempModifier;
        tempModifier = Instantiate(this) as GameSpaceModifier;
        tempModifier.m_isInstance = true;
        tempModifier.m_spaceOwner = owner;
        return tempModifier;
        
    }
}
