﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class GameSpace
{
    //Game space holds all the gameplay information about a given tile

    public bool isNull { get { return name == ""; } }
    public string name;
    public Vector3Int m_gridPosition
    {
        get { return new Vector3Int(m_posX, m_posY, m_posZ); }
        set
        {
            m_posX = value.x;
            m_posY = value.y;
            m_posZ = value.z;
        }
    }
    [HideInInspector]
    public int m_posX, m_posY, m_posZ;

    public List<GameObject> m_occupants = new List<GameObject>();
    public List<GameSpaceModifier> m_modifiers = new List<GameSpaceModifier>();
    public GameGrid m_gridOwner;

    //private void OnEnable()
    //{
    //    m_occupants = new List<GameObject>();
    //    m_modifiers = new List<GameSpaceModifier>();
    //}

}
