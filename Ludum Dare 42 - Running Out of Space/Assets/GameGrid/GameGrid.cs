﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class GameGrid : MonoBehaviour
{
    //The Game Grid keeps a matrix (nested Lists) of tile objects
    //A GridLayout is simply the data used for spacing and organizing the cells. It doesn't actually contain the cells themselves
    //We want the game grid to be nested arrays for fastest lookups

    public Grid m_grid;

    public Array3dGameSpace m_gameSpaces;
    public BoundsInt m_gridBounds;
    public List<Tilemap> m_tileMaps; //assumes map 0 is the main map
    public Vector3Int m_gameGridStartOffset; //The offset from the (0,0,0) tile of the grid layout. Used to convert grid layout to game space


    //Sets the offset
    public void CalculateOffset()
    {
        m_gameGridStartOffset = m_gridBounds.min * -1;
    }

    public GameSpace TilePositionToGameSpace(Vector3Int tilePosition)
    {
        Vector3Int gameSpaceIndex = tilePosition + m_gameGridStartOffset;
        if (IsIndexInRange(gameSpaceIndex)) return m_gameSpaces[gameSpaceIndex.x, gameSpaceIndex.y, gameSpaceIndex.z];
        else return null;
    }

    public Vector3Int GameSpacePositionToTilePosition(Vector3Int gameSpacePosition)
    {
        Vector3Int tilePosition = gameSpacePosition - m_gameGridStartOffset;
        return tilePosition;
    }

    public GameSpace WorldPositionToGameSpace(Vector3 worldPosition)
    {
        GameSpace returnedGameSpace = null;
        Vector3Int cellPosition = m_grid.WorldToCell(worldPosition);
        returnedGameSpace = TilePositionToGameSpace(cellPosition);

        return returnedGameSpace;
    }

    public Vector3 GameSpaceCenterToWorld(GameSpace gameSpace)
    {
        Vector3 returnedVector3 = Vector3.zero;
        Tilemap tempTilemap = m_grid.GetComponentInChildren<Tilemap>();
        if(tempTilemap == null)
        {
            Debug.LogWarning("GAME GRID - GameSpaceCenterToWorld - Could not find tilemap of associated grid");
        }
        else
        {
            returnedVector3 = tempTilemap.GetCellCenterWorld(gameSpace.m_gridPosition);
        }

        return returnedVector3;
    }

    public bool IsIndexInRange(Vector3Int index)
    {
        if (m_gameSpaces == null)
        {
            Debug.LogWarning("Game Grid - Index of game space being checked before the game grid has been created");
            return false;
        }
        if (0 <= index.x && index.x < m_gameSpaces.size.x &&
            0 <= index.y && index.y < m_gameSpaces.size.y &&
            0 <= index.z && index.z < m_gameSpaces.size.z)
        {
            return true;
        }
        else return false;
    }
}

[System.Serializable]
public class Array3dGameSpace : Array3d<GameSpace>
{
    public Array3dGameSpace(Vector3Int incSize) : base(incSize)
    {

    }
}



