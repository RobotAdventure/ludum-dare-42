﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Array3d<T>
{
    public T[] m_array;
    public Vector3Int size
    {
        get { return new Vector3Int(m_xSize, m_ySize, m_zSize); }
        set
        {
            m_xSize = value.x;
            m_ySize = value.y;
            m_zSize = value.z;
        }
    }

    [SerializeField]
    private int m_xSize;
    [SerializeField]
    private int m_ySize;
    [SerializeField]
    private int m_zSize;

    public Array3d(Vector3Int incSize)
    {
        m_array = new T[incSize.x * incSize.y * incSize.z];
        size = incSize;
    }

    public T this[int rowIndex, int colIndex, int zIndex]
    {
        get { return m_array[ConvertToIndex(new Vector3Int(rowIndex, colIndex, zIndex))]; }
        set { m_array[ConvertToIndex(new Vector3Int(rowIndex, colIndex, zIndex))] = value; }
    }

    private int ConvertToIndex(Vector3Int cord)
    {
        return cord.z * (m_xSize * m_ySize) + (cord.y * m_xSize) + cord.x;
    }
}
