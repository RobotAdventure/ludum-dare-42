﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenHandler : MonoBehaviour
{
    public FloatVariable m_oxygen;
    public float m_oxygenLossRate = 1.0f; //per second

    private void Update()
    {
        m_oxygen.value -= m_oxygenLossRate * Time.deltaTime;
    }

}
