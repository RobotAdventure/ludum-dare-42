﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Space/Space Spawn Factory", fileName = "Spawn Factory")]
public class SpaceSpawnFactory : ScriptableObject
{
    public List<ObjectSpawnStats> m_spawns = new List<ObjectSpawnStats>();

    public GameObject SpawnSpaceObject()
    {
        GameObject tempGO = GetRandomSpawn().m_spawnPrefab;
        if (tempGO == null) return null;
        else return Instantiate(tempGO);        
    }

    ObjectSpawnStats GetRandomSpawn()
    {
        ObjectSpawnStats returnSpawnStat = m_spawns[0];
        float totalWeight = 0.0f;

        foreach (ObjectSpawnStats statWeight in m_spawns) totalWeight += statWeight.m_weight;

        float randomFloat = Random.Range(0, totalWeight);
        totalWeight = 0.0f;
        foreach(ObjectSpawnStats stat in m_spawns)
        {
            totalWeight += stat.m_weight;
            if(totalWeight > randomFloat)
            {
                returnSpawnStat = stat;
                break;
            }
        }
        return returnSpawnStat;
    }
}

[System.Serializable]
public class ObjectSpawnStats
{
    public string name;
    public float m_weight;
    public GameObject m_spawnPrefab;
}

