﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceSpawner : MonoBehaviour
{
    public GameObject m_player;
    public GameObject m_startCenter;
    public GameObject currentCenter
    {
        get { return m_spaceChunkGrid[1, 1]; }
    }

    public GameObject m_backgroundPrefab;

    public SpaceSpawnFactory m_spaceSpawnFactory;    
    public GameObject[,] m_spaceChunkGrid = new GameObject[3, 3];
    [Space]
    public float m_chunkSize;
    public float m_chunkSpacing; //row and column height
    [Space]
    public Vector2 m_xVariation;
    [Space]
    public Vector2 m_yVariation;

    public void Start()
    {
        m_spaceChunkGrid[1, 1] = m_startCenter;
        SetupSpace();
    }

    private void Update()
    {
        //Check Player Position, if entered a new grid, shift everything
        CheckPlayerPosition();
    }

    public void SetupSpace()
    {
        //Set Center
        Vector3 centerPosition = currentCenter.transform.position;
        Vector3 gridChunkSize = new Vector3(m_chunkSize, m_chunkSize, 0);

        //Initial Spawn
        for (int x = 0; x < m_spaceChunkGrid.GetLength(0); x++)
        {
            for (int y = 0; y < m_spaceChunkGrid.GetLength(1); y++)
            {
                if (m_spaceChunkGrid[x, y] != null) continue; //If not null, it was already setup
                m_spaceChunkGrid[x, y] = CreateChunk();
                m_spaceChunkGrid[x, y].transform.position = centerPosition + Vector3.Scale(gridChunkSize, new Vector3(x - 1, y - 1, 0));
            }
        }
    }

    public GameObject CreateChunk()
    {
        GameObject spaceChunk = new GameObject("Space Chunk");
        spaceChunk.transform.parent = this.transform;
        //Put Background In
        GameObject background = Instantiate(m_backgroundPrefab);
        background.GetComponent<SpriteRenderer>().size = new Vector2(m_chunkSize, m_chunkSize);
        background.transform.parent = spaceChunk.transform;
        background.transform.localPosition = Vector3.zero;


        Vector3 tempPosition;
        Vector3 chunkCenterOffset = new Vector3(.5f * m_chunkSize, .5f * m_chunkSize, 0);
        GameObject tempGO;
        //Chunk is a cube, so we see how many spaces are in it's x,y grid based off of chunk spacing
        int sectionSize = (int)Mathf.Floor(m_chunkSize / m_chunkSpacing);
        for(int x = 0; x < sectionSize; x++)
        {
            for(int y = 0; y < sectionSize; y++)
            {
                tempPosition = new Vector3(x * m_chunkSpacing, y * m_chunkSpacing) + GetOffsetFromVariations(m_xVariation, m_yVariation); //Get position
                tempPosition += -chunkCenterOffset;
                tempGO = m_spaceSpawnFactory.SpawnSpaceObject();
                if (tempGO == null) continue;

                tempGO.transform.parent = spaceChunk.transform;
                tempGO.transform.localPosition = tempPosition;
            }
        }

        return spaceChunk;
    }

    Vector3 GetOffsetFromVariations(Vector2 xVariation, Vector2 yVariation)
    {
        float x = Random.Range(xVariation.x, xVariation.y);
        float y = Random.Range(yVariation.x, yVariation.y);
        return new Vector3(x, y, 0);
    }

    void CheckPlayerPosition()
    {
        //Check Player Position, if entered a new grid, shift everything
        Vector3 playerOffset = m_player.transform.position - currentCenter.transform.position;
        float halfChunkSize = m_chunkSize * .5f;
        if (playerOffset.x > halfChunkSize)
        {
            ShiftCenterRight();
            SetupSpace();
        }
        else if (playerOffset.x < -halfChunkSize)
        {
            ShiftCenterLeft();
            SetupSpace();
        }
        else if (playerOffset.y > halfChunkSize)
        {
            ShiftCenterUp();
            SetupSpace();
        }
        else if (playerOffset.y < -halfChunkSize)
        {
            ShiftCenterDown();
            SetupSpace();
        }
    }

    void ShiftCenterRight()
    {
        for (int x = 0; x < m_spaceChunkGrid.GetLength(0); x++)
        {
            for (int y = 0; y < m_spaceChunkGrid.GetLength(1); y++)
            {
                if(x == m_spaceChunkGrid.GetUpperBound(0)) //we're on the edge, null it out
                {                    
                    m_spaceChunkGrid[x, y] = null;
                }
                else
                {
                    if (x == 0) Destroy(m_spaceChunkGrid[x, y]);
                    m_spaceChunkGrid[x, y] = m_spaceChunkGrid[x + 1, y];
                }
            }
        }
    }
    void ShiftCenterLeft()
    {
        for (int x = m_spaceChunkGrid.GetUpperBound(0); x >= 0; x--)
        {
            for (int y = 0; y < m_spaceChunkGrid.GetLength(1); y++)
            {
                if (x == 0) //we're on the edge, null it out
                {
                    m_spaceChunkGrid[x, y] = null;
                }
                else
                {
                    if (x == m_spaceChunkGrid.GetUpperBound(0)) Destroy(m_spaceChunkGrid[x, y]);
                    m_spaceChunkGrid[x, y] = m_spaceChunkGrid[x -1, y];
                }
            }
        }
    }

    void ShiftCenterUp()
    {
        for (int x = 0; x < m_spaceChunkGrid.GetLength(0); x++)
        {
            for (int y = 0; y < m_spaceChunkGrid.GetLength(1); y++)
            {
                if (y == m_spaceChunkGrid.GetUpperBound(1)) //we're on the edge, null it out
                {
                    m_spaceChunkGrid[x, y] = null;
                }
                else
                {
                    if (y == 0) Destroy(m_spaceChunkGrid[x, y]);
                    m_spaceChunkGrid[x, y] = m_spaceChunkGrid[x, y + 1];
                }
            }
        }
    }
    void ShiftCenterDown()
    {
        for (int x = 0; x < m_spaceChunkGrid.GetLength(0); x++)
        {
            for (int y = m_spaceChunkGrid.GetUpperBound(1); y >= 0; y--)
            {
                if (y == 0) //we're on the edge, null it out
                {
                    m_spaceChunkGrid[x, y] = null;
                }
                else
                {
                    if (y == m_spaceChunkGrid.GetUpperBound(1)) Destroy(m_spaceChunkGrid[x, y]);
                    m_spaceChunkGrid[x, y] = m_spaceChunkGrid[x, y - 1];
                }
            }
        }
    }
}


