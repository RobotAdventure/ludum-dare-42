﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthWithShield : Health
{
    public FloatVariable m_shield;

    public override void TakeDamage(int damage)
    {
        if (isInvuln) return;
        int tempDamage = damage;
        if (m_shield.value > damage)
        {
            m_shield.value -= damage;
            base.TakeDamage(0);
        }
        else
        {
            tempDamage -= (int)(m_shield.value);
            m_shield.value = 0;

            base.TakeDamage(tempDamage);
        }
    }

}
