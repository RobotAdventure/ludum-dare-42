﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumptionHandler : MonoBehaviour
{
    public BasicGlobalGameEvent m_consumptionEvent;
    public FloatVariable m_shield;
    public FloatVariable m_oxygen;
    public FloatVariable m_fuel;
    public CargoTypeSO m_consumedType;

    public void OnEnable()
    {
        m_consumptionEvent.m_event += ConsumeCargo;
    }

    private void OnDisable()
    {
        m_consumptionEvent.m_event -= ConsumeCargo;
    }

    public void ConsumeCargo(GlobalGameEventInfo info)
    {
        CargoObject consumedCargo = ((GameObject)info.m_targetGameObject).GetComponent<CargoObject>();

        switch(consumedCargo.m_cargoType.m_type)
        {
            case CargoObjectTypes.Energy:
                m_shield.value += consumedCargo.m_amount;
                break;

            case CargoObjectTypes.Fuel:
                m_fuel.value += consumedCargo.m_amount;
                break;

            case CargoObjectTypes.Oxygen:
                m_oxygen.value += consumedCargo.m_amount;
                break;                
        }

        consumedCargo.m_cargoType = m_consumedType;
        consumedCargo.ApplyColor();

    }

}
