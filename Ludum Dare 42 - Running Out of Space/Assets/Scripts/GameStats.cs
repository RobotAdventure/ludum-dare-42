﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Space/Game Stats", fileName = "GameStats")]
public class GameStats : ScriptableObject
{
    public FloatVariable m_health;
    public FloatVariable m_fuel;
    public FloatVariable m_shields;
    public FloatVariable m_oxygen;
    

}
