﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : EntityPart
{
    public GameStats m_gameStats;
    public Rigidbody2D m_rigidBody;
    public float m_thrusterStrength = 100.0f; //force per second
    public float m_maxSpeed = 5.0f; //Meters per second
    public float m_speedMag = 0.0f;
    public float m_fuelUsage = 1.5f; //Fuel used per second of boosting
    public FloatVariable m_fuel;

    private bool m_braking = false;

    //Thrusters
    public GameObject m_thrusterL;
    public GameObject m_thrusterR;

    private void Awake()
    {
        if (m_rigidBody == null) GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Look at Mouse Cursor
        LookAtCursor();

        //Check for Brake
        if (Input.GetKeyDown(KeyCode.Space)) m_braking = true;
        else if (Input.GetKeyUp(KeyCode.Space)) m_braking = false;


        //If Click, accelerate Forward
        if (Input.GetMouseButton(0)) ForwardThrust();
        //if (Input.GetKey(KeyCode.Space)) BrakeThrusters();

        m_speedMag = m_rigidBody.velocity.magnitude;

        //Show/Hide Thrusters
        if (Input.GetMouseButtonDown(0)) SetThrusters(true);
        else if (Input.GetMouseButtonUp(0)) SetThrusters(false);
    }

    private void LookAtCursor()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0.0f;
        transform.up = (mousePosition - transform.position).normalized;
    }

    private void ForwardThrust()
    {
        if (m_braking) return;
        if(m_fuel.value > 0.0f)
        {
            m_rigidBody.AddForce(transform.up * m_thrusterStrength);
            if (m_rigidBody.velocity.magnitude > m_maxSpeed)
            {
                m_rigidBody.velocity = Vector3.ClampMagnitude(m_rigidBody.velocity, m_maxSpeed);
            }
            m_fuel.value -= m_fuelUsage * Time.deltaTime;
        }
        
    }

    private void SetThrusters(bool visible)
    {
        m_thrusterR.SetActive(visible);
        m_thrusterL.SetActive(visible);
    }

    private void BrakeThrusters()
    {
        if (m_rigidBody.velocity.magnitude > m_thrusterStrength * Time.deltaTime) m_rigidBody.AddForce(-m_rigidBody.velocity * m_thrusterStrength * 1.5f);
        else m_rigidBody.velocity = Vector3.zero;
    }
}
