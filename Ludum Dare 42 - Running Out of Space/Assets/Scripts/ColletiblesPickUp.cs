﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColletiblesPickUp : MonoBehaviour
{

    public BasicGlobalGameEvent m_event;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            m_event.Raise(new GlobalGameEventInfo(this, collision.gameObject));
            Destroy(this.gameObject);
        }
    }
}
