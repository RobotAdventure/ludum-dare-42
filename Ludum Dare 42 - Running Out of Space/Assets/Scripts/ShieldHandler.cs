﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldHandler : MonoBehaviour
{
    public FloatVariable m_shield;
    public float m_shieldRechargeRate = .75f;//persecond

	// Use this for initialization
	void Update ()
    {
        m_shield.value += m_shieldRechargeRate * Time.deltaTime;	
	}
	
}
