﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Space/Tutorial Menu %m", fileName = "Tutorial")]
public class TutorialMenuSO : ScriptableObject
{
    public Texture m_picture;

    [TextArea]
    public string m_text;
    
}
