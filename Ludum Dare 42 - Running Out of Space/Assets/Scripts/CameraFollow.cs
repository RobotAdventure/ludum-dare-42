﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    public Transform m_followTarget;
    public float m_distanceFromEdge; //Distance from the target center to the edge of the screen
    public float m_smoothTime = .3f;

    private float m_followOffset;
    private float m_cameraZ;
    private Vector3 m_cameraVelocity = Vector3.zero;
    private Camera m_cameraRef;

    private void Start()
    {
        m_cameraZ = transform.position.z;
        m_cameraRef = GetComponent<Camera>();
        m_followOffset = m_cameraRef.orthographicSize * .5f - m_distanceFromEdge;
    }


    // Update is called once per frame
    void FixedUpdate ()
    {
        HandleFollow();

    }

    private void HandleZoom()
    {

    }
    
    private void HandleFollow()
    {
        Vector3 targetPosition = m_followTarget.transform.position + (m_followTarget.transform.up * m_followOffset);
        targetPosition.z = m_cameraZ;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref m_cameraVelocity, m_smoothTime);
    }

    private void CalculateOffset()
    {
        m_followOffset = (m_cameraRef.orthographicSize * .5f) - m_distanceFromEdge;
    }
}
