﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowOscillation : MonoBehaviour
{
    public float m_osicllationSpeedModifier = 1.0f;
    public float m_scaleRange = .5f;

    private void Update()
    {
        transform.localScale = Vector3.one * (1 + m_scaleRange * Mathf.Sin(m_osicllationSpeedModifier * Time.time));
    }
}
