﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatBarWatcher : MonoBehaviour
{
    public Slider m_slider;
    public FloatVariableRange m_floatVariable;

    public void Start()
    {
        UpdateBar();        
    }

    public void OnEnable()
    {
        m_floatVariable.m_event += GetVariableChange;
    }

    private void OnDisable()
    {
        m_floatVariable.m_event -= GetVariableChange;
    }

    public void GetVariableChange(GlobalGameEventInfo info)
    {
        UpdateBar();
    }

    private void UpdateBar()
    {
        m_slider.minValue = m_floatVariable.m_runtimeMin;
        m_slider.maxValue = m_floatVariable.m_runtimeMax;
        m_slider.value = m_floatVariable.value;
    }



}
