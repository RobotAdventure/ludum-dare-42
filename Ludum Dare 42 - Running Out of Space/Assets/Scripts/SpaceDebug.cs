﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceDebug : MonoBehaviour
{
    public FloatVariable m_variable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftBracket)) m_variable.value -= 5.0f;
        if (Input.GetKeyDown(KeyCode.RightBracket)) m_variable.value += 5.0f;
    }
}
