﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CargoObject : MonoBehaviour
{
    public CargoTypeSO m_cargoType;
    public BasicGlobalGameEvent m_cargoCloseEvent;

    public bool isSecured
    {
        get
        {
            bool returnSecure = true;
            foreach (CargoObjectPiece piece in m_pieces)
            {
                if (piece.m_attachedSpace.isNull) returnSecure = false;
                break;
            }
            return returnSecure;

        }
    }


    private CargoObjectPiece[] m_pieces;

    [Range(0,100)]
    public int m_amount;

    public void Awake()
    {
        m_pieces = GetComponentsInChildren<CargoObjectPiece>();

    }

    private void OnEnable()
    {
        m_cargoCloseEvent.m_event += CargoClosed;
    }

    private void OnDisable()
    {
        m_cargoCloseEvent.m_event -= CargoClosed;
    }

    public void CargoClosed(GlobalGameEventInfo info)
    {
        if(isSecured == false)
        {
            Destroy(this.gameObject);
        }
    }

    public void ApplyColor()
    {
        CargoObjectPiece[] m_pieces = GetComponentsInChildren<CargoObjectPiece>();
        SpriteRenderer tempRenderer;
        foreach(CargoObjectPiece piece in m_pieces)
        {
            tempRenderer = piece.GetComponent<SpriteRenderer>();
            tempRenderer.color = m_cargoType.m_cargoColor;
        }
    }

    

}

public enum CargoObjectTypes
{
    Energy = 0,
    Fuel,
    Oxygen,
    Passenger,
    Consumed
}




