﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoInput : MonoBehaviour
{
    public Camera m_cargoCamera;
    public GameObject m_currentCargoHighlight;
    public GameObject m_currentDraggedCargo;
    public Vector3 m_draggedLocalAnchor;
    public LayerMask m_cargoLayerMask;
    public Vector3 m_startDragPosition;
    public Quaternion m_startDragRotation;
    public GameGrid m_cargoGameGrid;

    public BasicGlobalGameEvent m_consumptionEvent;

    public CargoObjectPiece[] m_draggedPieces;

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) CargoHoverCheck();

        

        if(m_currentDraggedCargo == null)
        {
            CargoHoverCheck();
            if (Input.GetMouseButtonDown(0))
            {
                StartCargoDrag();
            }

            if(Input.GetMouseButtonDown(1))
            {
                ConsumeHihglight();
            }
        }


        if (m_currentDraggedCargo != null) //currently in drag mode
        {
            HandleDrag();
            if (Input.GetKeyDown(KeyCode.E)) RotateObject(1);
            if (Input.GetKeyDown(KeyCode.Q)) RotateObject(-1);
            if (Input.GetKeyDown(KeyCode.T)) Destroy(m_currentDraggedCargo.gameObject);

            if (Input.GetMouseButtonUp(0))
            {
                HandleDragRelease();
            }
        }
    }

    GameObject CargoHoverCheck()
    {
        //Converting Mouse Pos to 2D (vector2) World Pos
        Vector2 rayPos = new Vector2(m_cargoCamera.ScreenToWorldPoint(Input.mousePosition).x, m_cargoCamera.ScreenToWorldPoint(Input.mousePosition).y);
        RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f, m_cargoLayerMask);
        CargoObject tempCargoObject;

        if (hit)//&& hit.transform.tag == "Cargo")
        {
            tempCargoObject = hit.transform.GetComponent<CargoObject>();
            if(tempCargoObject != null && tempCargoObject.m_cargoType.m_type == CargoObjectTypes.Consumed)
            {
                m_currentCargoHighlight = null;
                return null;
            }
            //Debug.Log(hit.transform.name);
            m_currentCargoHighlight = hit.transform.parent.gameObject;
            return m_currentCargoHighlight;
        }
        else
        {
            m_currentCargoHighlight = null;
            return null;
        }
    }

    void StartCargoDrag()
    {
        if (m_currentCargoHighlight == null) return;
        //Check if consumed, if so, don't pick up
        CargoObject tempCargoObject = m_currentCargoHighlight.GetComponent<CargoObject>();
        if (tempCargoObject.m_cargoType.m_type == CargoObjectTypes.Consumed) return;

        m_currentDraggedCargo = m_currentCargoHighlight;
        m_startDragPosition = m_currentDraggedCargo.transform.position;
        m_startDragRotation = m_currentDraggedCargo.transform.rotation;
        m_draggedLocalAnchor = m_currentCargoHighlight.transform.InverseTransformPoint(GetCameraXY());
        m_currentDraggedCargo = m_currentCargoHighlight;
        m_draggedPieces = m_currentDraggedCargo.GetComponentsInChildren<CargoObjectPiece>();

        foreach (CargoObjectPiece piece in m_draggedPieces) piece.StartDrag(m_cargoGameGrid);
    }

    void HandleDrag()
    {
        Vector3 anchorOffset = m_currentDraggedCargo.transform.TransformPoint(m_draggedLocalAnchor) - m_currentDraggedCargo.transform.position;
        m_currentDraggedCargo.transform.position = GetCameraXY() - anchorOffset;

        //Change highlights based off of game space occupation
        //Run through array and calculate highlight
        foreach(CargoObjectPiece piece in m_draggedPieces)
        {
            piece.CheckHighlightSpace();
        }
        //TODO Have the object handle itself so we dont' waste time traversing the list
    }

    void HandleDragRelease()
    {

        bool isClear = true;
        bool isConsumed = false;

        foreach (CargoObjectPiece piece in m_draggedPieces)
        {
            if (piece.isSpaceFree == false)
            {
                m_currentDraggedCargo.transform.position = m_startDragPosition;
                m_currentDraggedCargo.transform.rotation = m_startDragRotation;
                isClear = false;            
            }

            if (piece.isSpaceConsumption)
            {
                isConsumed = true;
                isClear = false;
                break;
            }
            
        }

        //Object is consumed
        if(isConsumed)
        {
            //Raise Trigger
            m_consumptionEvent.Raise(new GlobalGameEventInfo(this, m_currentDraggedCargo));

        }

        foreach (CargoObjectPiece piece in m_draggedPieces)
        {
            //check highlights. If area clear, drop cargo and occupy spaces;
            //If area isn't clear, return the cargo to it's original position and occupy spaces
            if(isClear) piece.CheckHighlightSpace();
            piece.EndDrag(isClear);            
        }
        if (isClear) SnapCargoToGrid(m_currentDraggedCargo);

        m_currentDraggedCargo = null;

    }

    void SnapCargoToGrid(GameObject cargo)
    {
        CargoObjectPiece piece = cargo.GetComponentInChildren<CargoObjectPiece>();
        Vector3 pieceGameSpaceWorld = m_cargoGameGrid.GameSpaceCenterToWorld(piece.m_attachedSpace);
        Vector3 offset = pieceGameSpaceWorld - piece.transform.position;
        cargo.transform.position += offset;
    }

    void RotateObject(int direction)
    {
        m_currentDraggedCargo.transform.RotateAround(m_currentDraggedCargo.transform.TransformPoint(m_draggedLocalAnchor), Vector3.forward, 90 * direction);
            //Vector3.forward, direction * 90);
    }

    Vector3 GetCameraXY()
    {
        Vector3 tempV3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return new Vector3(tempV3.x, tempV3.y, 0);
    }

    void ConsumeHihglight()
    {
        CargoObject cargoObject;
        if(m_currentCargoHighlight != null)
        {
            cargoObject = m_currentCargoHighlight.GetComponent<CargoObject>();
            if(cargoObject != null && cargoObject.m_cargoType.m_type != CargoObjectTypes.Consumed && cargoObject.isSecured)
            {
                m_consumptionEvent.Raise(new GlobalGameEventInfo(this, m_currentCargoHighlight));
            }
        }
    }

}
