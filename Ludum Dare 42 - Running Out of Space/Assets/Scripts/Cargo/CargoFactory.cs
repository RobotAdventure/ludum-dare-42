﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Space/Cargo Factory", fileName = "Cargo Factory")]
public class CargoFactory : ScriptableObject
{
    [System.Serializable]
    public class CargoTier
    {
        public string m_name;
        public float m_weight;
        public List<GameObject> m_tierPrefabs;

        public GameObject GetRandomFromList()
        {
            return m_tierPrefabs[Random.Range(0, m_tierPrefabs.Count)];
        }
    }

    public List<CargoTier> m_tiers;

    public List<CargoTypeSO> m_cargoTypes;

    public GameObject GetRandomCargo()
    {
        //Find a tier through a random weight
        float totalWeight = 0;
        foreach (CargoTier tier in m_tiers) totalWeight += tier.m_weight;
        float weightResult = Random.Range(0, totalWeight);
        CargoTier targetTier = GetTierByWeight(weightResult);

        //Create the game Object and find the Cargo Object script attached
        GameObject tempCargoGO = Instantiate(targetTier.GetRandomFromList());
        CargoObject tempCargoObject = tempCargoGO.GetComponent<CargoObject>();

        //Choose a cargo type and assign and apply it to the cargo
        tempCargoObject.m_cargoType = m_cargoTypes[Random.Range(0, m_cargoTypes.Count)];
        tempCargoObject.ApplyColor();

        return tempCargoGO;
    }

    public CargoTier GetTierByWeight(float number)
    {
        float escalatingWeight = 0;
        foreach(CargoTier tier in m_tiers)
        {
            escalatingWeight += tier.m_weight;
            if(escalatingWeight > number)
            {
                return tier;
            }
        }
        return m_tiers[m_tiers.Count - 1]; //default return biggest
    }

}


