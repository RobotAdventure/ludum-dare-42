﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Space/Cargo Type", fileName = "Cargo Type")]
public class CargoTypeSO : ScriptableObject
{
    public CargoObjectTypes m_type;
    public Color m_cargoColor;

}
