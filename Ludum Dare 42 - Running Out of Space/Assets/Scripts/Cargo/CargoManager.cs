﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoManager : MonoBehaviour
{
    public bool m_cargoOpen = false;
    public Camera m_shipCamera;
    public Camera m_cargoCamera;
    public BasicGlobalGameEvent m_cargoClosedEvent;

    public Rigidbody2D m_shipRigid;
    private Vector3 m_shipStoredVelocity;

    public GameObject m_cargoVisuals;
    public GameObject m_cargoObjects;

    public GameObject [] m_cargoAddSpots;
    public BasicGlobalGameEvent m_cargoPickUpEvent;
    public CargoFactory m_factory;

	// Use this for initialization
	void Start ()
    {
        m_cargoPickUpEvent.m_event += CargoPickedUp;
	}

    private void OnDisable()
    {
        m_cargoPickUpEvent.m_event -= CargoPickedUp;
    }

    // Update is called once per frame
    void Update ()
    {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            if (m_cargoOpen) CloseCargo();
            else OpenCargo();

        }
        
	}


    public void OpenCargo()
    {
        m_cargoOpen = true;
        m_cargoVisuals.SetActive(true);
        m_cargoObjects.SetActive(true);

        m_shipCamera.gameObject.SetActive(false);
        m_cargoCamera.gameObject.SetActive(true);

        m_shipStoredVelocity = m_shipRigid.velocity;
        m_shipRigid.velocity = Vector3.zero;
        m_shipRigid.gameObject.SetActive(false);
    }

    public void CloseCargo()
    {

        m_cargoClosedEvent.Raise(new GlobalGameEventInfo(this));
        m_cargoOpen = false;
        m_cargoVisuals.SetActive(false);
        m_cargoObjects.SetActive(false);

        m_cargoCamera.gameObject.SetActive(false);
        m_shipCamera.gameObject.SetActive(true);

        m_shipRigid.gameObject.SetActive(true);
        m_shipRigid.velocity = m_shipStoredVelocity;

        
     
    }

    public void CargoPickedUp(GlobalGameEventInfo info)
    {
        AddCargo(m_factory.GetRandomCargo());
        OpenCargo();
    }

    public void AddCargo(GameObject cargo)
    {
        GameObject spawnPoint = m_cargoAddSpots[Random.Range(0, m_cargoAddSpots.Length)];
        cargo.transform.parent = m_cargoObjects.transform;
        GameObject tempCargoPieceGO = cargo.transform.GetChild(0).gameObject;
        cargo.transform.position = spawnPoint.transform.position;
        Vector3 cargoOffset = spawnPoint.transform.position - tempCargoPieceGO.transform.position;
        cargo.transform.position += cargoOffset;
    }

}
