﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CargoObjectPiece : MonoBehaviour
{
    public Color m_openHighlightColor;
    public Color m_occupiedHighlightColor;
    public Color m_consumptionColor;

    public SpriteRenderer m_highlight;
    public GameGrid m_gameGrid;
    public GameSpace m_attachedSpace = null;
    public GameSpace m_highlightSpace;
    public bool m_dragActive = false;
    public bool m_consumed = false;

    //Modifiers
    public GameSpaceModifier m_unplaceableModifier;
    public GameSpaceModifier m_consumptionModifier;

    public bool isSpaceFree
    {
        get
        {
            if (m_highlightSpace == null) return false;
            if((m_highlightSpace.m_occupants.Count == 0 || m_highlightSpace == m_attachedSpace) && !m_highlightSpace.m_modifiers.Contains(m_unplaceableModifier))
            {
                return true;
            }
            return false;
        }
    }

    public bool isSpaceConsumption
    {
        get
        {
            if (m_highlightSpace == null) return false;
            if (m_highlightSpace.m_modifiers.Contains(m_consumptionModifier)) return true;
            else return false;
        }
    }




    public void Update()
    {
        if(m_dragActive)
        {
            CheckHighlightSpace();
        }
    }

    public void StartDrag(GameGrid gameGrid)
    {
        m_dragActive = true;
        m_highlight.gameObject.SetActive(true);
        m_gameGrid = gameGrid;
        m_attachedSpace.m_occupants.Remove(transform.parent.gameObject);
    }

    public void EndDrag(bool occupySpace)
    {
        m_dragActive = false;
        m_highlight.gameObject.SetActive(false);

        if (occupySpace)
        {
            //remove from olds pace
            m_attachedSpace.m_occupants.Remove(transform.parent.gameObject);

            //Occupy new space
            m_attachedSpace = m_highlightSpace;
            m_attachedSpace.m_occupants.Add(transform.parent.gameObject);
        }

        m_gameGrid = null;
    }

    public void CheckHighlightSpace()
    {
        if (m_highlight == null) return;

        m_highlightSpace = m_gameGrid.WorldPositionToGameSpace(transform.position);
        if (isSpaceConsumption) m_highlight.color = m_consumptionColor;
        else if (isSpaceFree) m_highlight.color = m_openHighlightColor;       
        else m_highlight.color = m_occupiedHighlightColor;
    }

}
