﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRandomizer : MonoBehaviour
{
    public SpriteRenderer m_renderer;
    public Sprite[] m_sprites;

    private void Awake()
    {
        int random = Random.Range(0, m_sprites.Length);
        m_renderer.sprite = m_sprites[random];

        Vector3 euler = transform.eulerAngles;
        euler.z = Random.Range(0f, 360f);
        transform.eulerAngles = euler;

        gameObject.AddComponent<PolygonCollider2D>();
    }

}
